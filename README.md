# PACTL - Sym

## Descrição
Repositório de documentação para o projeto de desenvolvimento do parser de PDDL para BDDs.

## Documentação
O site de documentação do projeto pode ser encontrado [aqui](https://pactl-sym-labxp-ime-usp-2024-1-pactl-6e30bbe1f76ac247ed32d018b5.gitlab.io). 

## Equipe
- Matheus Sanches Jurgensen (Coach).
- André Nogueira Ribeiro (Tracker).
- Henri Michel França Oliveira.
- João Guilherme Alves Santos.

## Kanban
O Kanban do projeto está exposto em uma lousa na sala 2 do CEC, mas pode ser também acessado digitalmente [aqui](https://gitlab.com/groups/labxp-ime-usp/2024.1/pactl/-/boards).

## Planilha de Horas
A planilha de horas do projeto pode ser encontrada [aqui](https://docs.google.com/spreadsheets/d/1BOlVsayh3nefgkEE4cjS3C1_eRGZGZ28emWS_NNSTe4/edit?usp=sharing).