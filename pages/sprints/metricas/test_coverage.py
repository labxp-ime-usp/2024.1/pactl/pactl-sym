import matplotlib.pyplot as plt
import numpy as np

coverages = [81,73,75]


x_values = ["sprint1","sprint2", "sprint3"]

# Create the plot
plt.figure(figsize=(10, 6))
plt.plot(x_values, coverages, marker='o', linestyle='-', color='blue', label='Tempo esperado')

# Add labels and title
plt.xlabel('Sprint')
plt.ylabel('Porcentagem do código')
plt.title('Porcentagem de cobertura de testes após cada sprint')
plt.legend()  # Show the legend

# Show the plot
plt.grid(axis='y', linestyle='--')
plt.savefig('cobertura_testes.png')