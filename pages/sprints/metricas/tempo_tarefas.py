import matplotlib.pyplot as plt
import numpy as np

def generate_graph(tempos):
    """
    Plots two lines on a graph using the values in the given lists.

    Args:
        list1: The list of y-values for the first line.
        list2: The list of y-values for the second line.
    """
    list1 = tempos[0]
    list2 = tempos[1]

    subtraction = np.array(list2) - np.array(list1)


    x_values = ["sprint1","sprint2", "sprint3"]

    # Create the plot
    plt.figure(figsize=(10, 6))
    plt.plot(x_values, list1, marker='o', linestyle='-', color='blue', label='Tempo esperado')
    plt.plot(x_values, list2, marker='o', linestyle='-', color='red', label='Tempo gasto')
    plt.plot(x_values, subtraction, marker='x', linestyle='--', color='green', label='Diferença entre os tempos')

    # Add labels and title
    plt.xlabel('Sprint')
    plt.ylabel('Tempo em horas')
    plt.title('Horas esperadas e horas demandadas nas tarefas de cada sprints')
    plt.legend()  # Show the legend

    # Show the plot
    plt.grid(axis='y', linestyle='--')
    plt.savefig('tempo_tarefas.png')

def get_times(path):
    numbers = "1234567890"
    tempos_estimados = []
    tempos_gastos = []
    flag = 0

    with open(path) as file:
        lines = file.readlines()

        for i in range(len(lines)):
            tempo = ""
            if len(lines[i].strip()) > 4 and lines[i].strip()[4] in numbers:
                j = 4
                while len(lines[i].strip()) > j and lines[i].strip()[j] != "<":
                    if lines[i].strip()[j] == ",":
                        tempo += "."
                    else:
                        tempo += lines[i].strip()[j]
                    j += 1
                if flag % 2 == 0:
                    tempos_estimados.append(float(tempo))
                else:
                    tempos_gastos.append(float(tempo))
                flag += 1

    tempos = [tempos_estimados, tempos_gastos]

    return tempos

def main():
    paths = ["../sprint1.html", "../sprint2.html", "../sprint3.html"]
    aux1 = []
    aux2 = []
    pontos = []
    for path in paths:
        tempos = get_times(path)
        aux = []
        aux1.append(sum(tempos[0]))
        aux2.append(sum(tempos[1]))

    pontos.append(aux1)
    pontos.append(aux2)

    generate_graph(pontos)

if __name__ == "__main__":
    main()